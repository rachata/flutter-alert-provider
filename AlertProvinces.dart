import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:senior/model/Provinces.dart';
import 'package:senior/provider/answer.dart';

class AlertProvinces extends StatefulWidget {
  @override
  _AlertProvincesState createState() => _AlertProvincesState();
}

class _AlertProvincesState extends State<AlertProvinces> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        contentPadding: EdgeInsets.only(top: 10.0),
        content: FutureBuilder(
            future:
            Provider.of<AnswerModel>(context, listen: false).getProvinces(),
            builder: (BuildContext context, AsyncSnapshot<Provinces> snapshot) {
              if (snapshot.hasData) {
                return Container(
                  height: MediaQuery
                      .of(context)
                      .size
                      .height,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  child: ListView.separated(
                      separatorBuilder: (context, index) {
                        return Divider(
                          thickness: 0.3,
                          color: Colors.grey,
                        );
                      },
                      itemCount: snapshot.data.data.length,
                      itemBuilder: (ctx, i) {
                        return ListTile(
                          onTap: () => {
                          Navigator.pop(context, snapshot.data.data[i])


                        },
                          title: Text(
                            snapshot.data.data[i].nameInThai,
                            style: GoogleFonts.prompt(
                                fontSize: 16,
                                color: Colors.black,
                                ),
                          ),
                        );
                      }),
                );
              } else {
                return Container(
                  height: 100,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  child: Text(
                    "รอสักครู่",
                    style: GoogleFonts.prompt(

                        color: Colors.black,
                        fontSize: 16),
                  ),
                );
              }
            }));
  }
}
